import './App.css';
import React from 'react';
import ArticleList from "./components/ArticleList";
import articleapi from "./api/articleapi";

class App extends React.Component{
    state = {articles: [] }
    async componentDidMount() {
        const response = await articleapi.get('');
        this.setState({articles : response.data.data.articles})
    }
    render() {
        return (
            <div>
               <ArticleList articles={this.state.articles} />
            </div>
        );
    }


}

export default App;
